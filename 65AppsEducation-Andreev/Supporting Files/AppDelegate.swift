//
//  AppDelegate.swift
//  65AppsEducation-Andreev
//
//  Created by Илья Андреев on 08.10.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().tintColor = Colors.aeLabelGray
        UINavigationBar.appearance().backgroundColor = .white
//        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: Colors.aeLabelGray,
            NSAttributedString.Key.font: UIFont(name: Fonts.openSansRegular, size: 17)!
        ]
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(rootViewController: LoginViewController())
        return true
    }
}

