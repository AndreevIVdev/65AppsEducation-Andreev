//
//  Constants.swift
//  65AppsEducation-Andreev
//
//  Created by Илья Андреев on 14.10.2021.
//

import UIKit

enum Fonts {
    
    static let openSansRegular = "OpenSans-Regular"
    static let openSansRomanLight = "OpenSansRoman-Light"
    static let openSansRomanSemiBold = "OpenSansRoman-SemiBold"
    static let openSansRomanBold = "OpenSansRoman-Bold"
    static let openSansBold = "OpenSans-Bold"
    static let openSansRomanExtraBold = "OpenSansRoman-ExtraBold"
    static let openSansRomanCondensedRegular = "OpenSansRoman-CondensedRegular"
    static let openSansRomanCondensedLight = "OpenSansRoman-CondensedLight"
    static let openSansRomanCondensedSemiBold = "OpenSansRoman-CondensedSemiBold"
    static let openSansRomanCondensedBold = "OpenSansRoman-CondensedBold"
    static let openSansRomanCondensedExtraBold = "OpenSansRoman-CondensedExtraBold"
    
//    for family in UIFont.familyNames.sorted() {
//        let names = UIFont.fontNames(forFamilyName: family)
//        print("Family: \(family) Font names \(names)")
//    }
}

enum Colors {
    
    static let aewhite = UIColor(rgb: 0xE5E5E5)
    static let aeRed = UIColor(rgb: 0xE91D29)
    static let aeGray = UIColor(rgb: 0xDADADA)
    static let aeLabelGray = UIColor(rgb: 0x8B999F)
}
