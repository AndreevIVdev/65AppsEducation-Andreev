//
//  SendPasswordRecoveryViewController.swift
//  65AppsEducation-Andreev
//
//  Created by Илья Андреев on 18.10.2021.
//

import UIKit

class SendRecoveryCodeViewController: UIViewController {
    
    private let scrollView: UIScrollView = .init()
    private let contentView: UIView = .init()
    private let emailTextField: AETextField = .init()
    private let sendCodeButton: AEButton = .init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureScrollView()
        configureContentView()
        configureEmailTextField()
        configureSendCodeButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func configureViewController() {
        let backgroundView = UIView()
        backgroundView.backgroundColor = .white
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        ])
        title = "Password recovery"
        view.backgroundColor = Colors.aewhite
        registerKeyboardNotifications()
        view.addSubViews(scrollView)
        view.backgroundColor = Colors.aewhite
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: view,
                action: #selector(UIView.endEditing))
        )
    }
    
    private func configureScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        scrollView.addSubViews(contentView)
    }
    
    private func configureContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor),
            contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        contentView.addSubViews(
            emailTextField,
            sendCodeButton
        )
    }
    
    private func configureEmailTextField() {
        emailTextField.placeholder = "Email"
        emailTextField.keyboardType = .emailAddress
        emailTextField.returnKeyType = .go
        emailTextField.delegate = self
        
        NSLayoutConstraint.activate([
            emailTextField.heightAnchor.constraint(equalToConstant: 50),
            emailTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 250),
            emailTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 28),
            emailTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -27)
        ])
    }
    
    private func configureSendCodeButton() {
        sendCodeButton.addTarget(
            self,
            action: #selector(sendCodeButtonTapped),
            for: .touchUpInside
        )
        sendCodeButton.backgroundColor = Colors.aeRed
        sendCodeButton.setTitle("SEND CODE", for: .normal)
        
        NSLayoutConstraint.activate([
            sendCodeButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 28),
            sendCodeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -27),
            sendCodeButton.heightAnchor.constraint(equalToConstant: 50),
            sendCodeButton.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 33)
        ])
    }
    
    @objc private func sendCodeButtonTapped(){
        navigationController?.pushViewController(SaveNewPasswordViewController(), animated: true)
    }
}

extension SendRecoveryCodeViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch(textField) {
        case emailTextField:
            sendCodeButtonTapped()
        default:
            fatalError()
        }
        return true
    }
}

extension SendRecoveryCodeViewController {
    
    private func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    private func removeKeyboardNotification() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardFrameSize = (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        scrollView.contentOffset = CGPoint(x: 0, y: keyboardFrameSize.height / 2)
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        scrollView.contentOffset = CGPoint.zero
    }
}
