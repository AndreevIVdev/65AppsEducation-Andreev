//
//  UITextField+Ext.swift
//  65AppsEducation-Andreev
//
//  Created by Илья Андреев on 15.10.2021.
//

import Foundation
import UIKit

extension UITextField {
    var isEntered: Bool {
        !self.text!.isEmpty
    }
}
